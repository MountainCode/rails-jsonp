JSONP on Rails
==============

Running
-------

```bash
$ bundle install
$ rake db:migrate
$ rails c
```

In Rails console

```ruby
u = User.new
u.name = 'Chris Kentfield'
u.save
^D
```

Back in the shell...

```bash
$ ruby script/sinatra.rb &
$ rails s
```

Point your browser at http://localhost:4567

Important Parts
---------------

```ruby
class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    respond_to do |format|
      format.json { render :json => @user, :callback => params[:callback] }
    end
  end
end
```
