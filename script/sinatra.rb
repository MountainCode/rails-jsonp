require 'sinatra'

get '/' do
	<<-eof
  <!DOCTYPE html>
  <html>
    <head>
      <title>Rails JSONP Test</title>
      <script>
        function handleJsonP(data) {
          alert(data.name);
        }
        window.onload = function() {
          var script = document.createElement('script');
          script.src = 'http://localhost:3000/users/1.json?callback=handleJsonP';
          script.onload = function() {
            document.body.removeChild(script);
          }
          document.body.appendChild(script);
        };
      </script>
    </head>
    <body>
    </body>
  </html>
  eof
end
